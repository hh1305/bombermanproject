/*
    Thêm board cho portal
*/
package uet.oop.bomberman.level;

import java.io.BufferedReader;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.LayeredEntity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.enemy.Balloon;
import uet.oop.bomberman.entities.tile.Grass;
import uet.oop.bomberman.entities.tile.Portal;
import uet.oop.bomberman.entities.tile.Wall;
import uet.oop.bomberman.entities.tile.destroyable.Brick;
import uet.oop.bomberman.entities.tile.item.BombItem;
import uet.oop.bomberman.entities.tile.item.FlameItem;
import uet.oop.bomberman.entities.tile.item.SpeedItem;
import uet.oop.bomberman.exceptions.LoadLevelException;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import uet.oop.bomberman.entities.character.enemy.Doll;
import uet.oop.bomberman.entities.character.enemy.Minvo;
import uet.oop.bomberman.entities.character.enemy.Oneal;

public class FileLevelLoader extends LevelLoader {

	/**
	 * Ma trận chứa thông tin bản đồ, mỗi phần tử lưu giá trị kí tự đọc được
	 * từ ma trận bản đồ trong tệp cấu hình
	 */
	private static char[][] _map;
	
	public FileLevelLoader(Board board, int level) throws LoadLevelException, FileNotFoundException {
		super(board, level);
	}
	
	@Override
	public void loadLevel(int level) throws LoadLevelException {
		// TODO: đọc dữ liệu từ tệp cấu hình /levels/Level{level}.txt
       try {
            String pathFileLevel = "res/levels/Level" + Integer.toString(level) + ".txt";
            Scanner reader = new Scanner(new File(pathFileLevel));
            _level = reader.nextInt();
            _height = reader.nextInt();
            _width = reader.nextInt();
            System.out.println(reader.nextLine().length());
            _map = new char[_height][_width];
            for (int i = 0; i < _height; i++) {
                _map[i] = reader.nextLine().substring(0, _width).toCharArray();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // TODO: cập nhật các giá trị đọc được vào _width, _height, _level, _map
	}

	@Override
	public void createEntities() {
		// TODO: tạo các Entity của màn chơi
		// TODO: sau khi tạo xong, gọi _board.addEntity() để thêm Entity vào game

		// TODO: phần code mẫu ở dưới để hướng dẫn cách thêm các loại Entity vào game
		// TODO: hãy xóa nó khi hoàn thành chức năng load màn chơi từ tệp cấu hình
		for (int x = 0; x < _width; x++) {
			for (int y = 0; y < _height; y++) {
			    switch (_map[y][x]) {
                    case '#':
                        addWall(x, y);
                        break;
                    case 'p':
                        addBomber(x, y);
                        break;
                    case '1':
                        addBalloon(x, y);
                        break;
                    case '2':
                        addOneal(x, y);
                        break;
                    case '3':
                        addDoll(x, y);
                        break;
                    case '4':
                        addMinvo(x, y);
                        break;
                    case '5':
                        addKondoria(x, y);
                        break;
                    case '*':
                        addBrick(x, y);
                        break;
                    case 'f':
                        addFlameIteam(x, y);
                        break;
                    case 'b':
                        addBombIteam(x, y);
                        break;
                    case 's':
                        addSpeedIteam(x, y);
                        break;
                    case 'x':
                        addPortal(x, y);
                        break;
                    default:
                        addGrass(x, y);
                        break;
                }
			}
        }
	}

    private void addPortal(int x, int y) {
        _board.addEntity(x + y * _width,
                new LayeredEntity(x, y,
                        new Grass(x, y, Sprite.grass),
                        new Portal(x, y, Sprite.portal, _board),
                        new Brick(x,y,Sprite.brick)));
    }

    private void addFlameIteam(int xI, int yI) {
        _board.addEntity(xI + yI * _width,
                new LayeredEntity(xI, yI,
                        new Grass(xI ,yI, Sprite.grass),
                        new FlameItem(xI, yI, Sprite.powerup_flames),
                        new Brick(xI, yI, Sprite.brick)
                )
        );
    }

    private void addSpeedIteam(int xI, int yI) {
        _board.addEntity(xI + yI * _width,
                new LayeredEntity(xI, yI,
                        new Grass(xI ,yI, Sprite.grass),
                        new SpeedItem(xI, yI, Sprite.powerup_speed),
                        new Brick(xI, yI, Sprite.brick)
                )
        );
    }

    private void addBombIteam(int xI, int yI) {
        _board.addEntity(xI + yI * _width,
                new LayeredEntity(xI, yI,
                        new Grass(xI ,yI, Sprite.grass),
                        new BombItem(xI, yI, Sprite.powerup_bombs),
                        new Brick(xI, yI, Sprite.brick)
                )
        );
    }

    private void addGrass(int x, int y) {
        int pos = x + y * _width;
        _board.addEntity(pos, new Grass(x, y, Sprite.grass));
    }

    private void addWall(int x, int y) {
        int pos = x + y * _width;
        _board.addEntity(pos, new Wall(x, y, Sprite.wall));
    }

    private void addBomber(int xBomber, int yBomber) {
        _board.addCharacter( new Bomber(Coordinates.tileToPixel(xBomber), Coordinates.tileToPixel(yBomber) + Game.TILES_SIZE, _board) );
        Screen.setOffset(0, 0);
        _board.addEntity(xBomber + yBomber * _width, new Grass(xBomber, yBomber, Sprite.grass));
    }

    private void addBalloon(int xE, int yE) {
        _board.addCharacter( new Balloon(Coordinates.tileToPixel(xE), Coordinates.tileToPixel(yE) + Game.TILES_SIZE, _board));
        _board.addEntity(xE + yE * _width, new Grass(xE, yE, Sprite.grass));
    }
    
    private void addOneal(int xO, int yO){
        _board.addCharacter(new Oneal(Coordinates.tileToPixel(xO), Coordinates.tileToPixel(yO) + Game.TILES_SIZE, _board));
        _board.addEntity(xO + yO * _width, new Grass(xO, yO, Sprite.grass));
    }
    
    // add Doll, Minvo, Kondoria
    private void addDoll(int xD, int yD){
        _board.addCharacter(new Doll(Coordinates.tileToPixel(xD) ,Coordinates.tileToPixel(yD) + Game.TILES_SIZE, _board));
        _board.addEntity(xD + yD * _width, new Grass(xD, yD, Sprite.grass));
    }
    
    private void addMinvo(int xM, int yM){
        _board.addCharacter(new Minvo(Coordinates.tileToPixel(xM), Coordinates.tileToPixel(yM) + Game.TILES_SIZE, _board));
        _board.addEntity(xM + yM * _width, new Grass(xM, yM, Sprite.grass));
    }
    
    private void addKondoria(int xK, int yK){
        _board.addCharacter(new Minvo(Coordinates.tileToPixel(xK), Coordinates.tileToPixel(yK) + Game.TILES_SIZE, _board));
        _board.addEntity(xK + yK * _width, new Grass(xK, yK, Sprite.grass));
    }

    private void addBrick(int xB, int yB) {
        _board.addEntity(xB + yB * _width,
                new LayeredEntity(xB, yB,
                        new Grass(xB, yB, Sprite.grass),
                        new Brick(xB, yB, Sprite.brick)
                )
        );
    }

}