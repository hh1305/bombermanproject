/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman.socket;

import org.json.simple.JSONValue;
import java.io.*;
import java.net.*;

/**
 *
 * @author user
 */
public class Socket {

    private static java.net.Socket data;
    public static BufferedReader in;
    public static PrintWriter out;

    public static void openBuffer() {
        try {
            in = new BufferedReader(new InputStreamReader(data.getInputStream()));
            out = new PrintWriter(data.getOutputStream(), true);
        }
        catch (Exception e) {
            System.out.println("IOexception when openBuffer");
            e.printStackTrace();
        }
    }

    public static Object decodeData(String data) throws Exception{
        return JSONValue.parse(data);
    }

    public static String endcodeData(Object data) throws Exception{
        StringWriter jsonOut = new StringWriter();
        JSONValue.writeJSONString(data, jsonOut);
        return jsonOut.toString();
    }

    public static void closeBuffer() throws IOException{
        in.close();
        out.close();
    }

    public static void end() {
        try {
            closeBuffer();
            data.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void start(String server, int port) {

        try {
            InetAddress inetAddress = InetAddress.getByName(server);
            SocketAddress socketAddress = new InetSocketAddress(inetAddress, port);

            data = new java.net.Socket();

            int timeOutInMs = 10 * 1000;

            data.connect(socketAddress, timeOutInMs);

            openBuffer();

        } catch (SocketTimeoutException e) {
            System.out.println("Time out for waiting from server");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOexception while open Socket");
            e.printStackTrace();
        }

    }
}
