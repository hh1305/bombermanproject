/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman.socket;

import java.io.IOException;

/**
 *
 * @author user
 */
public class SendToSocket extends Thread{
    String command;
    Object obj;
    
    public SendToSocket(String command, Object obj){
        this.command = command;
        this.obj = obj;
    }
    public void run(){
        try {
            Socket.out.println(command + " " + Socket.endcodeData(obj));
        }
        catch (Exception e) {
            System.out.println("Error while send to socket ( don't care about that ): " + e.getMessage());
        }
    }
}
