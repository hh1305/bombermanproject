package uet.oop.bomberman.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;

public class Server {

    private int portNumber = 2345;
    private ServerSocket serverSocket;

    public Server() {

    }

    public Server(int portNumber) {
        this.portNumber = portNumber;
        try {
            serverSocket = new ServerSocket(portNumber);
            run();
            serverSocket.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        ObjectInputStream in;
        ObjectOutputStream out;
        try {
            while (true)
            {
                Socket socket1 = serverSocket.accept();
                // handle receive
                // handle new room
                // handle come in room
                Packet p0 = new Packet(0,"Hello client");

                out = new ObjectOutputStream(socket1.getOutputStream());
                out.writeObject(p0);
                in = new ObjectInputStream(socket1.getInputStream());
                Packet p1 = (Packet) in.readObject();
                System.out.println(p1.getType());

                in.close();
                out.close();
            }


        }
        catch (Exception e) {
            System.out.println("IOexception while server run");
            e.printStackTrace();
        }
    }
}
