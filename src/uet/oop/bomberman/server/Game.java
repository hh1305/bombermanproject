package uet.oop.bomberman.server;

import javax.sound.midi.Soundbank;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Game {

    public synchronized void move(Packet packet, Player player) {
        player.opponent.otherPlayerMoved(packet);
    }

    public class Player extends Thread {
        private int side = 0;
        private Socket socket;
        private ObjectInputStream in;
        private ObjectOutputStream out;
        private Player opponent;

        public Player(int side, Socket socket) {
            this.side = side;
            this.socket = socket;
            try {
                in = new ObjectInputStream(socket.getInputStream());
                out = new ObjectOutputStream(socket.getOutputStream());
                Packet p = new Packet(-1, "Welcome");
                out.writeObject(p);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void otherPlayerMoved(Packet packet) {
            try {
                out.writeObject(packet);
            }
            catch (IOException e) {
                System.out.println("Error tranfer " + e);
            }
        }

        public void setOpponent(Player opponent) {
            this.opponent = opponent;
        }

        public void run() {
            try {
                Packet p = new Packet(0, "Start " +String.valueOf(side));
                out.writeObject(p);

                // wait for socket from client
                while (true) {
                    System.out.println("Flag 1");
                    p = (Packet) in.readObject();
                    System.out.println(p.getData());
                    if (p.getType() == 2) {
                        move(p, this);
                    }
                }
            }
            catch (IOException e) {
                System.out.println("Player dead " + e);
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            finally {
                try{socket.close();}catch (IOException e) { e.printStackTrace();}
            }
        }
    }
}
