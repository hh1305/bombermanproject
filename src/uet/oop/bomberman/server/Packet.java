package uet.oop.bomberman.server;
import java.io.Serializable;

public class Packet implements Serializable {

    private int type;
    private String data;

    public Packet(int type, String data) {
        this.type = type;
        this.data = data;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
