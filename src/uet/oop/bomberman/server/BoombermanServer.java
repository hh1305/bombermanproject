package uet.oop.bomberman.server;

import java.net.ServerSocket;

public class BoombermanServer {

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(2345);
        try {
            while (true) {
                Game game = new Game();
                Game.Player player1 = game.new Player(0, serverSocket.accept());
                Game.Player player2 = game.new Player(1,serverSocket.accept());
                System.out.println("Flag 0");
                player1.setOpponent(player2);
                player2.setOpponent(player1);
                player1.start();
                System.out.println("Flag 10");
                player2.start();
            }
        }
        finally {
            serverSocket.close();
        }
    }
}
