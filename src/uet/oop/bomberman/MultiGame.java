/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman;

import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.gui.Frame;
import uet.oop.bomberman.input.Keyboard;
import uet.oop.bomberman.socket.SendToSocket;
import uet.oop.bomberman.socket.Socket;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author user
 */

/**
 * Tạo vòng lặp cho game, lưu trữ một vài tham số cấu hình toàn cục,
 * Gọi phương thức render(), update() cho tất cả các entity
 */
public class MultiGame extends Game{

    public static final int TILES_SIZE = 16,
            WIDTH = TILES_SIZE * (31 / 2),
            HEIGHT = 13 * TILES_SIZE;

    public static int SCALE = 3;

    public static final String TITLE = "BombermanGame";

    private static final int BOMBRATE = 1;
    private static final int BOMBRADIUS = 1;
    private static final double BOMBERSPEED = 1.0;

    public static final int TIME = 200;
    public static final int POINTS = 0;

    protected static int SCREENDELAY = 3;

    protected static int bombRate = BOMBRATE;
    protected static int bombRadius = BOMBRADIUS;
    protected static double bomberSpeed = BOMBERSPEED;


    protected int _screenDelay = SCREENDELAY;

    private Keyboard _input;
    private Keyboard _input2;
    private boolean _running = false;
    private boolean _paused = true;

    private Board _board;
    private Screen screen;
    private uet.oop.bomberman.gui.Frame _frame;

    private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
    private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();

    private int level = -1;
    private Socket socket;
    private final Object lock = new Object();


    public MultiGame(Frame frame)  throws Exception{
        _frame = frame;
        _frame.setTitle(TITLE);

        screen = new Screen(WIDTH, HEIGHT);
        _input = new Keyboard();
        _input2 = new Keyboard();


        ReadFromSocket socket = new ReadFromSocket();
        SendToSocket socket2 = new SendToSocket("hello", Integer.valueOf(123));
        socket2.start();
        socket.start();
        // create board here
        synchronized (lock) {
            while (level == -1) {
                lock.wait();
            }
        }
        System.out.println(level);
        _board = new Board(this, _input, _input2, screen, level);
        addKeyListener(_input);
    }



    private void renderGame() {
        BufferStrategy bs = getBufferStrategy();
        if(bs == null) {
            createBufferStrategy(3);
            return;
        }

        screen.clear();

        _board.render(screen);

        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = screen._pixels[i];
        }

        Graphics g = bs.getDrawGraphics();

        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        _board.renderMessages(g);

        g.dispose();
        bs.show();
    }

    // sence here
    private void renderScreen() {
        BufferStrategy bs = getBufferStrategy();
        if(bs == null) {
            createBufferStrategy(3);
            return;
        }

        screen.clear();

        Graphics g = bs.getDrawGraphics();

        _board.drawScreen(g);

        g.dispose();
        bs.show();
    }

    private void update() {
        _input.update();
        _board.update();
    }

    public void start() {


        _running = true;

        long  lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        final double ns = 1000000000.0 / 60.0; //nanosecond, 60 frames per second
        double delta = 0;
        int frames = 0;
        int updates = 0;
        requestFocus();
        while(_running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while(delta >= 1) {
                update();
                updates++;
                delta--;
            }

            if(_paused) {
                if(_screenDelay <= 0) {
                    _board.setShow(-1);
                    _paused = false;
                }

                renderScreen();
            } else {
                renderGame();
            }

            // fps here
            frames++;
            if(System.currentTimeMillis() - timer > 1000) {
                _frame.setTime(_board.subtractTime());
                _frame.setPoints(_board.getPoints());
                timer += 1000;
                _frame.setTitle(TITLE + " | " + updates + " rate, " + frames + " fps");
                updates = 0;
                frames = 0;

                if(_board.getShow() == 2)
                    --_screenDelay;
            }
        }

    }

    public static double getBomberSpeed() {
        return bomberSpeed;
    }

    public static int getBombRate() {
        return bombRate;
    }

    public static int getBombRadius() {
        return bombRadius;
    }

    public static void addBomberSpeed(double i) {
        bomberSpeed += i;
    }

    public static void addBombRadius(int i) {
        bombRadius += i;
    }

    public static void addBombRate(int i) {
        bombRate += i;
    }

    public void resetScreenDelay() {
        _screenDelay = SCREENDELAY;
    }

    public Board getBoard() {
        return _board;
    }

    public boolean isPaused() {
        return _paused;
    }

    public void pause() {
        _paused = true;
    }

    public class ReadFromSocket extends Thread {

        public ReadFromSocket(){
            try {
                Socket.start(InetAddress.getLocalHost().getHostAddress(), 2345);
            }
            catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        public void execute(String command, Object obj) {
            System.out.println(command + " " + obj);
            // handle obj
            Long integer;
            switch (command) {
                case "map":
                    integer = (Long) obj;
                    synchronized (lock) {
                        level = integer.intValue();
                        lock.notify();
                    }
                    break;
                case "move":
                    integer = (Long) obj;
                    switch (integer.intValue()) {
                        case 0:
                            _input2.up = true;
                            _board._opponent.calculateMove();
                            _input2.up = false;
                            break;
                        case 1:
                            _input2.right = true;
                            _board._opponent.calculateMove();
                            _input2.right = false;
                            break;
                        case 2:
                            _input2.down = true;
                            _board._opponent.calculateMove();
                            _input2.down = false;
                            break;
                        case 3:
                            _input2.left = true;
                            _board._opponent.calculateMove();
                            _input2.left = false;
                            break;
                        case 4:
                            _input2.space = true;
                           // _board._opponent.detectPlaceBomb();
                            _input2.space = false;
                            break;
                        default :
                            break;
                    }
                    _board._opponent._moving = false;
            }
        }

        @Override
        public void run() {
            try {
                String line;
                while ((line = Socket.in.readLine()) != null) {
                    int space_pos = line.indexOf(' ');
                    System.out.println(line);
                    if (space_pos != -1) {
                        try {
                            Object obj = Socket.decodeData(line.substring(space_pos + 1));
                            this.execute(line.substring(0, space_pos), obj);
                        }
                        catch (Exception e) {
                            System.out.println("Error while read from socket :" + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }
            catch (IOException e) {
                System.out.println("Error read " + e.getMessage());
                e.printStackTrace();
            }
            finally {
                try {
                    System.out.println("Socket end time");
                    Socket.end();
                } catch (Exception e) {
                    System.out.println("Error while close socket " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }
}