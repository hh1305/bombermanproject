/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import uet.oop.bomberman.gui.Frame;

/**
 *
 * @author user
 */
public class Help extends JMenu{
    
    Frame frame;
    
    public Help(Frame _frame){
        
        super("Help");
        
        // How To Play      
        JMenuItem _how = new JMenuItem("Hướng dẫn");
        _how.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));
        _how.addActionListener(new MenuActionListener(frame));
        add(_how);
        
        // About
        JMenuItem _about = new JMenuItem("About");
        _about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
        _about.addActionListener(new MenuActionListener(frame));
        add(_about);
    }
    
    class MenuActionListener implements ActionListener {
		public Frame _frame;
		public MenuActionListener(Frame frame) {
			_frame = frame;
		}
		
		  @Override
		public void actionPerformed(ActionEvent e) {
			  
			  if(e.getActionCommand().equals("Hướng dẫn")) {
				JOptionPane.showMessageDialog(null, "Di chuyển : W, A, S , D hoặc Up, Down, Right, Left\nĐặt bom : Space hoặc X", "Hướng dẫn chơi", JOptionPane.INFORMATION_MESSAGE);
			  }
				  
			  if(e.getActionCommand().equals("About")) {
				  JOptionPane.showMessageDialog(null, "Bài tập lớn môn OOP\nAuthor : Nguyễn Huy Hoàng + Dương Văn Hải Anh", "About", JOptionPane.INFORMATION_MESSAGE);
			  }
		  }
	}
}
