/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import uet.oop.bomberman.gui.*;

/**
 *
 * @author user
 */
public class Game extends JMenu{
    
    public Frame frame;
    
    public Game(Frame frame){
        super("Menu");
        this.frame = frame;
        
        // New Game
        JMenuItem newGame = new JMenuItem("New Game");
        newGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        newGame.addActionListener(new MenuActionListener(frame));
        add(newGame);
        
        // Pause       
        JMenuItem pause = new JMenuItem("Pause");
        pause.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
        pause.addActionListener(new MenuActionListener(frame));
        add(pause);
        
        // Resume
        JMenuItem resume = new JMenuItem("Resume");
        resume.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
        resume.addActionListener(new MenuActionListener(frame));
        add(resume);
        
        // Scores
        JMenuItem scores = new JMenuItem("High Scores");
        scores.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
        scores.addActionListener(new MenuActionListener(frame));
        add(scores);
    }
    
   class MenuActionListener implements ActionListener{
       public Frame _frame;
       public MenuActionListener(Frame frame){
           _frame = frame;
       }

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if(e.getActionCommand().equals("New Game")){
                try {
                    _frame.newGame();
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
            if(e.getActionCommand().equals("Pause")) {
				  _frame.pauseGame();
			  }
				  
            if(e.getActionCommand().equals("Resume")) {
				  _frame.resumeGame();
			  }
            if(e.getActionCommand().equals("High Scores")){
                 JOptionPane.showMessageDialog(null, "Đang cập nhật...", "High Scores", JOptionPane.INFORMATION_MESSAGE);
            }
        }
       
   }
}
