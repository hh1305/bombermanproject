/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman.gui.menu;

import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import uet.oop.bomberman.gui.Frame;
import uet.oop.bomberman.Game;

/**
 *
 * @author user
 */
public class Options extends JMenu{
    
    Frame _frame;
    
    public Options(Frame frame){
        super("Level");
        
        _frame = frame;
        
        // load lever multiplayer
        JMenuItem level0 = new JMenuItem("Level 0");
        level0.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, KeyEvent.CTRL_MASK));
        
        // load level 1
        JMenuItem level1 = new JMenuItem("Level 1");
        level1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.CTRL_MASK));
        level1.addActionListener(new MenuActionListener(frame));
        add(level1);
        
        // load level 2
        JMenuItem level2 = new JMenuItem("Level 2");
        level2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, KeyEvent.CTRL_MASK));
        level2.addActionListener(new MenuActionListener(frame));
        add(level2);
        
        // load level 3
        JMenuItem level3 = new JMenuItem("Level 3");
        level3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, KeyEvent.CTRL_MASK));
        level3.addActionListener(new MenuActionListener(frame));
        add(level3);
        
        // load level 4
        JMenuItem level4 = new JMenuItem("Level 4");
        level4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, KeyEvent.CTRL_MASK));
        level4.addActionListener(new MenuActionListener(frame));
        add(level4);
        
        // load level 5
        JMenuItem level5 = new JMenuItem("Level 5");
        level5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_5, KeyEvent.CTRL_MASK));
        level5.addActionListener(new MenuActionListener(frame));
        add(level5);
        
        
    }
    
    class MenuActionListener implements ActionListener {
		public Frame _frame;
		public MenuActionListener(Frame frame) {
			_frame = frame;
		}
		
		  @Override
		public void actionPerformed(ActionEvent e) {
			  
			  if(e.getActionCommand().equals("Level 0")) {
                              try {
                                  _frame.changeLevel(0);
                              } catch (FileNotFoundException ex) {
                                  System.out.println(ex.toString());
                              }
			  }
				  
			  if(e.getActionCommand().equals("Level 1")) {
				  try {
                                  _frame.changeLevel(1);
                              } catch (FileNotFoundException ex) {
                                  System.out.println(ex.toString());
                              }
			  }
                          
                          if(e.getActionCommand().equals("Level 2")) {
				  try {
                                  _frame.changeLevel(2);
                              } catch (FileNotFoundException ex) {
                                  System.out.println(ex.toString());
                              }
			  }
                          
                          if(e.getActionCommand().equals("Level 3")) {
				 try {
                                  _frame.changeLevel(3);
                              } catch (FileNotFoundException ex) {
                                  System.out.println(ex.toString());
                              }
			  }
                          
                          if(e.getActionCommand().equals("Level 4")) {
				  try {
                                  _frame.changeLevel(4);
                              } catch (FileNotFoundException ex) {
                                  System.out.println(ex.toString());
                              }
			  }
                          
                          if(e.getActionCommand().equals("Level 5")) {
				 try {
                                  _frame.changeLevel(5);
                              } catch (FileNotFoundException ex) {
                                  System.out.println(ex.toString());
                              }
			  }
		  }
	}
    
}
