/*
    Nguồn âm thanh : https://downloads.khinsider.com/game-soundtracks/album/bomberman-nes
 */
package uet.oop.bomberman.sound;


import javax.sound.sampled.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author user
 */
public class Sound {
    
    private static final String BACKGROUND = "res/sound/background.wav"; // khởi tạo game
    private static final String EXPLOSION = "res/sound/explosion.wav"; // nỔ bom
    private static final String GAMEOVER = "res/sound/gameover.wav"; // thua
    private static final String GETITEM = "res/sound/getItem.wav"; // get item
    private static final String MODDIE = "res/sound/mobdie.wav"; // quái chết
    private static final String PASS = "res/sound/pass.wav"; // qua màn
    
    private static File bg; //background
    private static File exp; // explosion
    private static File go; // game over
    private static File gi; // get item
    private static File md; // mod die
    private static File pa; // pass
    
    private static Clip _bg; //background
    private static Clip _exp; // explosion
    private static Clip _go; // game over
    private static Clip _gi; // get item
    private static Clip _md; // mod die
    private static Clip _pa; // pass
    
    private static long clipTime = 0;

    // Khoi tao
    public static void init(){
        bg = new File(BACKGROUND);
        exp = new File(EXPLOSION);
        go = new File(GAMEOVER);
        gi = new File(GETITEM);
        md = new File(MODDIE);
        pa = new File(PASS);
    }
    
    // play music
    // dung AudioSystem https://docs.oracle.com/javase/7/docs/api/javax/sound/sampled/AudioSystem.html
    public static void play(File _file, Clip _clip, int loop) throws Exception{      
        AudioInputStream _audio = AudioSystem.getAudioInputStream(_file);
        _clip = AudioSystem.getClip();
        _clip.open(_audio);
        _clip.loop(loop);
        _clip.start();
    }
    
    public static void playBackGround(){       
        try {
            play(bg, _bg, 100);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    public static void stopBackGround(){       
        try {
            if(_bg != null && _bg.isRunning()){
                _bg.stop();
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
      
    }
    
    public static void pauseBackGround(){
        clipTime = _bg.getMicrosecondPosition();
        System.out.println(clipTime);
        _bg.stop();
    }
    
    public static void resumeBackGround(){
        _bg.setMicrosecondPosition(clipTime);
        clipTime = 0;
    }
    
    
    
    public static void playExplosion(){       
        try {
            play(exp, _exp, 0);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    public static void playGetItem(){       
        try {
            play(gi, _gi, 0);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    public static void playGameOver(){       
        try {
            play(go, _go, 0);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    public static void playModDie(){       
        try {
            play(md, _md, 0);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    public static void playPass(){       
        try {
            play(pa, _pa, 0);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
