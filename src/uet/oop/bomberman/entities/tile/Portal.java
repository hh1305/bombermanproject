/*
    Xử lý qua màn - 10/11
*/
package uet.oop.bomberman.entities.tile;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sound.Sound;

public class Portal extends Tile {
	private Board _board;

	public Portal(int x, int y, Sprite sprite, Board board) {
		super(x, y, sprite);
		_board = board;
	}
	
	@Override
	public boolean collide(Entity e) {
		// TODO: xử lý khi Bomber đi vào
		if (e instanceof Bomber) {
			if (_board.detectNoEnemies() && e.getXTile() == getX() && e.getYTile() == getY()) {
                            try {
                                Sound.playPass();
                                _board.nextLevel();
                            } catch (FileNotFoundException ex) {
                                System.out.println(ex.toString());
                            }
			}
                        return true;

		}
		return false;
	}

}
