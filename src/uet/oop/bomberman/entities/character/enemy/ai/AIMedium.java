package uet.oop.bomberman.entities.character.enemy.ai;

import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.enemy.Enemy;

public class AIMedium extends AI {
	Bomber _bomber;
	Enemy _e;
	
	public AIMedium(Bomber bomber, Enemy e) {
		_bomber = bomber;
		_e = e;
	}

	@Override
	public int calculateDirection() {
            // TODO: cài đặt thuật toán tìm đường đi
            // hướng đi xuống/phải/trái/lên tương ứng với các giá trị 0/1/2/3
            
            int slower = random.nextInt(20);
            int faster = random.nextInt(20);
            
            // thỏa mãn lúc chạy nhanh lúc chạy chậm của đề bài
            _e.set_speed(Game.BOMBERSPEED - slower/100 + faster/100);
            
            if(_bomber == null) return random.nextInt(4);
            
            int _rand = random.nextInt(4);
            
            if(_rand == 0){
                int _row = calculateRowDirection();
                if(_row != -1) return _row;
                else{
                    return calculateColDirection();
                }
            } else if(_rand == 1){
                int _col = calculateColDirection();
                if(_col != -1) return _col;
                else{
                    return calculateRowDirection();
                }
            }
            else return random.nextInt(4);

        }
        
        // tính toán theo cột
        protected int calculateColDirection(){
            if(_bomber.getXTile() < _e.getXTile()) return 2;
            else if(_bomber.getXTile() > _e.getXTile()) return 1;
            
            return -1;
        }
        
        // tính toán theo hàng
        protected int calculateRowDirection(){
            if(_bomber.getYTile() < _e.getYTile()) return 0;
            else if(_bomber.getYTile() > _e.getYTile()) return 3;
            
            return -1;
        }

}
