/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uet.oop.bomberman.entities.character;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.entities.bomb.Bomb;
import uet.oop.bomberman.graphics.Sprite;

import java.util.List;
/**
 *
 * @author user
 */
public class Bomber2 extends Bomber {

    private List<Bomb> _bombs;

    public Bomber2(int x, int y, Board board) {
        super(x, y, board);
        _bombs = _board.getBombs();
        _input = _board.getInput2();
        _sprite = Sprite.player_right;
    }

    @Override
    public void update() {
        //clearBombs();
        if (!_alive) {
            afterKill();
            return;
        }
        if (_timeBetweenPutBombs < -7500) _timeBetweenPutBombs = 0;
        else _timeBetweenPutBombs--;
        animate();
    }

    @Override
    protected void afterKill() {
        if (_timeAfter > 0) --_timeAfter;
        else {
            _board.youWin();
        }
    }
}
